package homeworks.homework04.figures;

import lombok.Getter;
import lombok.Setter;

/** Абстрактный класс Figure */
public abstract class Figure {
    @Getter @Setter private int x;
    @Getter @Setter private int y;

    /**
     * Абстрактный метод вычисления периметра
     *
     * @return число типа double
     */
    public abstract double getPerimeter();
}
