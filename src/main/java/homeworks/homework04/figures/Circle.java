package homeworks.homework04.figures;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Вычисляет длину окружности и перемещает фигуру по оси координат */
@Getter
@Setter
@NoArgsConstructor
public class Circle extends Ellipse implements Movable {

    private double radius = 0;
    private double diameter = 0;

    /**
     * Метод возвращает периметр окружности
     *
     * @return возвращает число типа double, периметр окружности
     */
    @Override
    public double getPerimeter() {
        double perim;
        double pI = Math.PI;
        if ((getRadius() != 0) && (getDiameter() == 0)) {
            // P = 2PIR
            perim = 2 * pI * getRadius();
        } else if ((getRadius() == 0) && (getDiameter() != 0)) {
            // P = PId
            perim = pI * getDiameter();
        } else {
            perim = 0;
        }
        return perim;
    }

    /**
     * Метод перемещения фигуры по оси координат
     *
     * @param x ось абсцисс
     * @param y ось ординат
     */
    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
