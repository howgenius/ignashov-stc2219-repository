package homeworks.homework04.figures;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Вычисляет периметр прямоугольника */
@NoArgsConstructor
@Setter
@Getter
public class Rectangle extends Figure {
    private double sideA;
    private double sideB;

    /**
     * Метод вычисления прямоугольника
     *
     * @return число типа double, периметр прямоугольника
     */
    @Override
    public double getPerimeter() {
        return 2 * (getSideA() + getSideB());
    }
}
