/**
 * Package with: classes - Circle, Ellipse, Figure, Main, Rectangle, Square. interfaces - Movable.
 *
 * @author Maxim Ignashov (mailto:maxster062@yandex.ru)
 * @version 1.0
 * @since 07.10.22
 */
package homeworks.homework04.figures;
