package homeworks.homework04.figures;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Вычисляет периметр эллипса */
@NoArgsConstructor
@Getter
@Setter
public class Ellipse extends Figure {
    private double longAxis;
    private double shortAxis;

    /**
     * Метод возвращает периметр эллипса
     *
     * @return число типа double, периметр эллипса
     */
    @Override
    public double getPerimeter() {
        return 2
                * Math.PI
                * Math.sqrt((Math.pow(getLongAxis(), 2) + Math.pow(getShortAxis(), 2)) / 8);
    }
}
