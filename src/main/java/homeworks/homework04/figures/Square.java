package homeworks.homework04.figures;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** Вычисляет периметр и перемещает фигуру по координатам */
@NoArgsConstructor
@Getter
@Setter
public class Square extends Rectangle implements Movable {
    private double side;

    /**
     * Метод для вычисления периметра фигуры
     *
     * @return число типа double, периметр квадрата
     */
    @Override
    public double getPerimeter() {
        return 4 * getSide();
    }

    /**
     * Перемещение квадрата по оси координат
     *
     * @param x ось абсцисс
     * @param y ось ординат
     */
    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
