package homeworks.homework04.figures;

import java.util.*;
import utils.menu.Menu;
import utils.menu.MenuOption;

/** Класс Main, для запуска программы вычисления периметра и перемещения фигур */
public class Main {
    /**
     * Метод начала работы программы
     *
     * @param args массив String[]
     */
    public static void main(String[] args) {
        Menu mainMenu = new Menu();
        Menu subMenu = new Menu(1);
        Menu subMenu2 = new Menu(1);

        ArrayList<Figure> figuresArrayList = new ArrayList<>();
        figuresArrayList.add(new Circle());
        figuresArrayList.add(new Ellipse());
        figuresArrayList.add(new Rectangle());
        figuresArrayList.add(new Square());

        Scanner scanner = new Scanner(System.in);

        mainMenu.add(
                new MenuOption("1", "Периметр окружности") {
                    /** Возвращает меню параметров вычисления периметра окружности */
                    @Override
                    public void doAction() {
                        subMenu.loopUntilExit();
                    }
                });

        subMenu.add(
                new MenuOption("1", "Вычисление периметра по радиусу") {
                    /** Определение фигуры типа Circle и вычисление её периметра по радиусу */
                    @Override
                    public void doAction() {
                        printCirclePerimeterRadius(figuresArrayList, scanner);
                    }
                });

        subMenu.add(
                new MenuOption("2", "Вычисление периметра по диаметру") {
                    /** Определение фигуры типа Circle и вычисление её периметра по диаметру */
                    @Override
                    public void doAction() {
                        printCirclePerimeterDiam(figuresArrayList, scanner);
                    }
                });

        mainMenu.add(
                new MenuOption("2", "Периметр эллипса") {
                    /** Определение фигуры типа Ellipse и вычисление её периметра */
                    @Override
                    public void doAction() {
                        printEllipsePerimeter(figuresArrayList, scanner);
                    }
                });

        mainMenu.add(
                new MenuOption("3", "Периметр прямоугольника") {
                    /** Определение фигуры типа Rectangle и вычисление её периметра */
                    @Override
                    public void doAction() {
                        printRectanglePerimeter(figuresArrayList, scanner);
                    }
                });

        mainMenu.add(
                new MenuOption("4", "Периметр квадрата") {
                    /** Определение фигуры типа Square и вычисление её периметра */
                    @Override
                    public void doAction() {
                        printSquarePerimeter(figuresArrayList, scanner);
                    }
                });

        mainMenu.add(
                new MenuOption("5", "Переместить окружность / квадрат") {
                    /** Возвращает меню выбора параметров перемещения фигур */
                    @Override
                    public void doAction() {
                        subMenu2.loopUntilExit();
                    }
                });

        subMenu2.add(
                new MenuOption("1", "Окружность") {
                    /** Определение фигуры типа Circle и перемещение по оси координат */
                    @Override
                    public void doAction() {
                        replaceFigure(figuresArrayList, "figures.Circle", scanner);
                    }
                });

        subMenu2.add(
                new MenuOption("2", "Квадрат") {
                    /** Определение фигуры типа Square и перемещение по оси координат */
                    @Override
                    public void doAction() {
                        replaceFigure(figuresArrayList, "figures.Square", scanner);
                    }
                });

        mainMenu.loopUntilExit();
    }

    private static void replaceFigure(
            ArrayList<Figure> figuresArrayList, String anObject, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals(anObject)) {
                        moveFigure(figure, scanner);
                    }
                });
    }

    private static void printSquarePerimeter(ArrayList<Figure> figuresArrayList, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals("figures.Square")) {
                        System.out.print("Введите значение стороны: ");
                        ((Square) figure).setSide(scanner.nextDouble());
                        System.out.println("Периметр квадрата = " + figure.getPerimeter());
                    }
                });
    }

    private static void printRectanglePerimeter(
            ArrayList<Figure> figuresArrayList, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals("figures.Rectangle")) {
                        System.out.print("Введите значение стороны A: ");
                        ((Rectangle) figure).setSideA(scanner.nextDouble());
                        System.out.print("Введите значение стороны B: ");
                        ((Rectangle) figure).setSideB(scanner.nextDouble());
                        System.out.println("Периметр прямоугольника = " + figure.getPerimeter());
                    }
                });
    }

    private static void printEllipsePerimeter(ArrayList<Figure> figuresArrayList, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals("figures.Ellipse")) {
                        System.out.print("Введите значение длинной оси эллипса: ");
                        ((Ellipse) figure).setLongAxis(scanner.nextDouble());
                        System.out.print("Введите значение короткой оси эллипса: ");
                        ((Ellipse) figure).setShortAxis(scanner.nextDouble());
                        System.out.println("Длина окружности эллипса = " + figure.getPerimeter());
                    }
                });
    }

    private static void printCirclePerimeterDiam(
            ArrayList<Figure> figuresArrayList, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals("figures.Circle")) {
                        ((Circle) figure).setRadius(0);
                        System.out.print("Введите диаметр окружности: ");
                        ((Circle) figure).setDiameter(scanner.nextDouble());
                        System.out.println("Периметр окружности = " + figure.getPerimeter());
                    }
                });
    }

    private static void printCirclePerimeterRadius(
            ArrayList<Figure> figuresArrayList, Scanner scanner) {
        figuresArrayList.forEach(
                figure -> {
                    if (figure.getClass().getName().equals("figures.Circle")) {
                        ((Circle) figure).setDiameter(0);
                        System.out.print("Введите радиус окружности: ");
                        ((Circle) figure).setRadius(scanner.nextDouble());
                        System.out.println("Периметр окружности = " + figure.getPerimeter());
                    }
                });
    }

    /**
     * Перемещает фигуру по координатам
     *
     * @param figure объект фигуры
     * @param scanner передача сканера для ввода значений
     */
    public static void moveFigure(Figure figure, Scanner scanner) {
        System.out.print("Текущие значения X и Y: ");
        System.out.println(figure.getX() + ":" + figure.getY());
        System.out.print("Введите значение X: ");
        int x = scanner.nextInt();
        System.out.print("Введите значение Y: ");
        int y = scanner.nextInt();
        if (figure.getClass().getName().equals("figures.Circle")) {
            ((Circle) figure).move(x, y);
        } else {
            ((Square) figure).move(x, y);
        }
        System.out.print("Окружность перемещена по X и Y: ");
        System.out.println(figure.getX() + ":" + figure.getY());
    }
}
