package homeworks.homework04.figures;

/** Интерфейс для перемещения фигуры по оси X и Y */
public interface Movable {
    void move(int x, int y);
}
