package homeworks.homework.calculator;

/** Выполняет преобразования из одной системы счисления в другую */
public class MakeCalculation {
    /**
     * Выполняет преобразование из двоичной системы счисления в десятичную
     *
     * @param inputBin число типа String, для разбора посимвольно
     */
    public void fromBinToDec(String inputBin) {
        char[] chars = inputBin.toCharArray();
        int result = 0;
        int mult = 1;
        for (int i = inputBin.length() - 1; i >= 0; i--) {
            if (chars[i] == '1') {
                result += mult;
            }
            mult *= 2;
        }
        System.out.println("\n" + inputBin + "(2) " + "-> " + result + "(10) \n");
    }

    /**
     * Выполняет преобразование из десятичной системы счисления в двоичную
     *
     * @param inputDecimal число типа String, будет преобразован в int
     */
    public void fromDecToBin(String inputDecimal) {
        int decimal = Integer.parseInt(inputDecimal);

        StringBuilder strBin = new StringBuilder();

        if (decimal < 0) {
            decimal = Math.abs(decimal);
        }
        while (decimal != 0) {
            strBin.append(decimal & 1);
            decimal >>>= 1; // побитовый сдвиг без знака
        }

        System.out.println(
                "\n"
                        + inputDecimal
                        + "(10) "
                        + "-> "
                        + Integer.parseInt(strBin.reverse().toString())
                        + "(2) \n");
    }
}
