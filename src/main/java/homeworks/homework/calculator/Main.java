package homeworks.homework.calculator;

import java.util.Scanner;
import utils.menu.Menu;
import utils.menu.MenuOption;

public class Main {
    /**
     * Метод начала работы программы
     *
     * @param args массив String[]
     */
    public static void main(String[] args) {
        System.out.println(
                "*Калькулятор преобразования из Десятичной системы счисления в Двоичную*");
        System.out.println();

        Menu mainMenu = new Menu();

        Scanner scanner = new Scanner(System.in);
        MakeCalculation mc = new MakeCalculation();

        mainMenu.add(
                new MenuOption("1", "О программе") {
                    /** Вызов действия меню "О программе" */
                    @Override
                    public void doAction() {
                        System.out.println(
                                "Программа предназначена для преобразования из двоичной системы"
                                        + " счисления \n"
                                        + " в десятичную и наоборот (ver. 2.0.0)");
                    }
                });

        Menu subMenu = new Menu(1);

        subMenu.add(
                new MenuOption("1", "Преобразовать из двоичной в десятичную") {
                    /** Возвращает число в десятичной системе счисления */
                    @Override
                    public void doAction() {
                        System.out.println("\n Введите число в двоичной системе счисления:");
                        mc.fromBinToDec(scanner.nextLine());
                    }
                });

        subMenu.add(
                new MenuOption("2", "Преобразовать из десятичной в двоичную") {
                    /** Возвращает число в двоичной системе счисления */
                    @Override
                    public void doAction() {
                        System.out.println("\n Введите число в десятичной системе счисления:");
                        mc.fromDecToBin(scanner.nextLine());
                    }
                });

        mainMenu.add(
                new MenuOption("2", "Произвести вычисления") {
                    /** Возвращает меню выбора параметров вычисления */
                    @Override
                    public void doAction() {
                        subMenu.loopUntilExit();
                    }
                });

        mainMenu.loopUntilExit();
    }
}
