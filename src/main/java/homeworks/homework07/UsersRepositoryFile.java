package homeworks.homework07;

public interface UsersRepositoryFile {

    User findById(int id);

    void create(User user);

    void update(User user);

    void delete(int id);
}
