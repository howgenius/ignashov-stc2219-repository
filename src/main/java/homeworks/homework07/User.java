package homeworks.homework07;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean isWorking;

    public User(String name, String lastName, int age, boolean isWorking) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.isWorking = isWorking;
    }

    @Override
    public String toString() {
        return id
                + insertSeparator()
                + name
                + insertSeparator()
                + lastName
                + insertSeparator()
                + age
                + insertSeparator()
                + isWorking;
    }

    private String insertSeparator() {
        return "|";
    }
}
