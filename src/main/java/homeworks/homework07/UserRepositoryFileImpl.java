package homeworks.homework07;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class UserRepositoryFileImpl implements UsersRepositoryFile {

    private final File constFILE = new File("src/main/resources/repository.txt");
    List<User> usersList = new ArrayList<>();
    int id = 0;

    @Override
    public User findById(int id) {
        readFile();
        return usersList.stream()
                .filter(currentUser -> currentUser.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Введённый id не найден!"));
    }

    @Override
    public void create(User user) {
        readFile();
        usersList.add(user);
        writeFile();
    }

    @Override
    public void update(User user) {
        readFile();
        usersList.set(user.getId() - 1, user);
        writeFile();
    }

    @Override
    public void delete(int id) {
        readFile();
        usersList.stream()
                .filter(currentUser -> currentUser.getId() == id)
                .findFirst()
                .orElseThrow(
                        () ->
                                new IllegalArgumentException(
                                        "Идентификатор пользователя не найден!"));
        usersList.remove(usersList.get(id - 1));
        writeFile();
    }

    private void determineId() {
        if (usersList.size() == 0 || usersList.get(0) == null) {
            usersList.clear();
        } else {
            id = usersList.get(usersList.size() - 1).getId();
        }
    }

    private void readFile() {
        if (constFILE.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(constFILE))) {
                usersList = getUsers(reader);
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        determineId();
    }

    private static List<User> getUsers(BufferedReader reader) {
        return reader.lines()
                .map(
                        readerUser -> {
                            String[] userInfo = readerUser.split("\\|");
                            if (!userInfo[0].equals("")) {
                                int tempId = Integer.parseInt(userInfo[0]);
                                String name = userInfo[1];
                                String lastName = userInfo[2];
                                int age = Integer.parseInt(userInfo[3]);
                                boolean isWorking = Boolean.parseBoolean(userInfo[4]);
                                User currentUser = new User(name, lastName, age, isWorking);
                                currentUser.setId(tempId);

                                return currentUser;
                            }
                            return null;
                        })
                .collect(Collectors.toList());
    }

    private void writeFile() {
        if (constFILE.delete()) {
            try (Writer writer = new FileWriter(constFILE)) {
                if ((constFILE.createNewFile()) || constFILE.exists()) {
                    int tempId = id;
                    usersList.forEach(
                            currentUser -> {
                                try {
                                    if (currentUser.getId() == 0) {
                                        int currentId = tempId + 1;
                                        currentUser.setId(currentId);
                                    }
                                    writer.write(currentUser + "\n");
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            });
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
