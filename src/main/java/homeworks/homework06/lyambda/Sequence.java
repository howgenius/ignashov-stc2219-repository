package homeworks.homework06.lyambda;

/**
 * Задача: <a
 * href="https://gitlab.com/igonin.oleg/stc-22-19/-/blob/main/Homeworks.md#6-%D0%BB%D1%8F%D0%BC%D0%B1%D0%B4%D0%B0-%D0%B2%D1%8B%D1%80%D0%B0%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F">
 * Ссылка на задачу</a>
 */
public class Sequence {
    /**
     * Возвращает массив четных чисел, если число нечетное, то число в массиве 0.
     *
     * @param array входящий массив
     * @return массив четных чисел
     */
    public int[] arrayCheckEven(int[] array) {
        return filter(array, currentCondition -> currentCondition % 2 == 0);
    }

    /**
     * Проверяет сумму элементов массива на чётность, если число нечетное, то число в массиве 0.
     *
     * @param array входящий массив
     * @return массив четных чисел
     */
    public int[] arrayCheckSumAtEven(int[] array) {
        return filter(array, Sequence::checkSumAtEven);
    }

    private static boolean checkSumAtEven(int currentCondition) {

        boolean isEven;
        int leftNumber;
        int rightNumber;

        leftNumber = currentCondition / 10;
        if (leftNumber != 0) {
            rightNumber = currentCondition % 10;
            isEven = (leftNumber + rightNumber) % 2 == 0;
        } else {
            isEven = currentCondition % 2 == 0;
        }
        return isEven;
    }

    /**
     * Разделяет число на цифры и проверяет каждую на чётность, если число нечетное, то число в
     * массиве 0.
     *
     * @param array входящий массив
     * @return массив четных чисел
     */
    public int[] arrayCheckAllNumbersAtEven(int[] array) {
        return filter(array, Sequence::checkAllNumbersAtEven);
    }

    private static boolean checkAllNumbersAtEven(int currentCondition) {
        boolean isEven;
        int leftNumber;
        int rightNumber;

        leftNumber = currentCondition / 10;
        if (leftNumber != 0) {
            rightNumber = currentCondition % 10;
            isEven = leftNumber % 2 == 0;
            if (!isEven) {
                isEven = rightNumber % 2 == 0;
            }
        } else {
            isEven = currentCondition % 2 == 0;
        }

        return isEven;
    }

    /**
     * Проверяет элемент массива на палиндромность, если число не является палиндромом, то число в
     * массиве 0.
     *
     * @param array входящий массив
     * @return массив четных чисел
     */
    public int[] arrayCheckAllNumbersPalindrom(int[] array) {

        return filter(array, Sequence::checkAllNumbersPalindrom);
    }

    private static boolean checkAllNumbersPalindrom(int currentCondition) {
        String result = Integer.toString(currentCondition);
        StringBuilder builder = new StringBuilder(result);
        String reverseResult = builder.reverse().toString();

        return result.equals(reverseResult);
    }

    /**
     * Статический метод возвращает массив чисел, элементы которого удовлетворяют определённому
     * условию
     *
     * @param array входящий массив
     * @param condition элемент интерфейса
     * @return массив чисел
     */
    private static int[] filter(int[] array, ByCondition condition) {
        int[] arrayResult = new int[array.length];

        for (int i = 0; i < arrayResult.length; i++) {
            if (condition.isOk(array[i])) {
                arrayResult[i] = array[i];
            }
        }
        return arrayResult;
    }
}
