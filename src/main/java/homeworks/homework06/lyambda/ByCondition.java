package homeworks.homework06.lyambda;

/** Интерфейс определения состояния */
public interface ByCondition {
    boolean isOk(int number);
}
