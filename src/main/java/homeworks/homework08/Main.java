package homeworks.homework08;

import homeworks.homework08.entity.Student;
import homeworks.homework08.entity.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();

        configuration.configure("/hibernate.cfg.xml");

        try (SessionFactory sessionFactory = configuration.buildSessionFactory()) {

            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            Student firstStudent = Student.builder()
                    .name("Maxim")
                    .build();

            Student secondStudent = Student.builder()
                    .name("Vladimir")
                    .build();

            Teacher firstTeacher = Teacher.builder()
                    .name("Oleg")
                    .subject("Lection")
                    .build();

            Teacher secondTeacher = Teacher.builder()
                    .name("Razil")
                    .subject("Practice")
                    .build();

            List<Teacher> firstTeachers = new ArrayList<>();
            firstTeachers.add(firstTeacher);
            firstTeachers.add(secondTeacher);
            firstStudent.setTeachers(firstTeachers);

            List<Teacher> secondTeachers = new ArrayList<>();
            secondTeachers.add(secondTeacher);
            secondStudent.setTeachers(secondTeachers);

            session.save(firstStudent);
            session.save(secondStudent);

            transaction.commit();

            session.close();
        }
    }
}
