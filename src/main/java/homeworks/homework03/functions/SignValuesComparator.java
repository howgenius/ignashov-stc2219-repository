package homeworks.homework03.functions;

import java.util.Comparator;

/**
 * Перемещение значимых элементов влево.
 *
 * @version 2.0
 */
public class SignValuesComparator implements Comparator<Integer> {
    /**
     * Метод вызывается при сравнении элементов
     *
     * @param o1 первый числовой параметр
     * @param o2 второй числовой параметр
     * @return число, -1, 0 или 1, в зависимости от условия метода
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        int returnValue;

        if ((o1 != 0) && (o2 == 0)) {
            returnValue = -1;
        } else if ((o1 == 0) && (o2 == 0)) {
            returnValue = 0;
        } else {
            returnValue = 1;
        }
        return returnValue;
    }
}
