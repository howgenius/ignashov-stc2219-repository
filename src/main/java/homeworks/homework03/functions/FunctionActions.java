package homeworks.homework03.functions;

import java.util.ArrayList;
import java.util.List;

/** Методы выполняющие действия работы с массивами */
public class FunctionActions {
    /**
     * Печатает найденный индекс в массиве
     *
     * @param arrayUtils объект класса ArrayUtils
     */
    public static void printFoundedIndex(ArrayUtils arrayUtils) {
        System.out.println("Индекс искомого числа: " + arrayUtils.getFoundedIndex(new int[10]));
    }

    /**
     * Перемещает ненулевые значения
     *
     * @param arrayUtils объект класса ArrayUtils
     */
    public static void replaceNotZeroValues(ArrayUtils arrayUtils) {
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(34);
        arrayList.add(0);
        arrayList.add(0);
        arrayList.add(14);
        arrayList.add(15);
        arrayList.add(0);
        arrayList.add(18);
        arrayList.add(0);
        arrayList.add(0);
        arrayList.add(1);
        arrayList.add(20);
        arrayUtils.getReplacedNumbers(arrayList);
    }
}
