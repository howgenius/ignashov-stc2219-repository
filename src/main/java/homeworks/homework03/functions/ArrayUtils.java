package homeworks.homework03.functions;

import java.util.*;
import java.util.stream.Collectors;
import lombok.NoArgsConstructor;

/**
 * Методы для работы с массивами, находит элемент по индексу, смещает значимые элементы массива
 * влево
 */
@NoArgsConstructor
public class ArrayUtils {

    private int foundedIndex;

    /**
     * Метод находит индекс элемента массива
     *
     * @param array пустой массив
     * @return возвращает числовой индекс массива
     */
    public int getFoundedIndex(int[] array) {
        List<Integer> arrayList = randomStreamGenerator(array);
        System.out.print("Текущие значения массива: ");
        System.out.println(arrayList.toString());
        System.out.print("Введите число, индекс которого найти в массиве: ");
        Scanner scanner = new Scanner(System.in);
        searchArrayNumber(scanner.nextInt(), arrayList);

        return foundedIndex;
    }

    /**
     * Метод перемещает значимые элементы массива влево
     *
     * @param arrayList пустой массив данных
     */
    public void getReplacedNumbers(List<Integer> arrayList) {
        String arrayString = replaceNumbers(arrayList);
        System.out.println("Значения массива после сдвига:");
        System.out.println(arrayString);
    }

    private void searchArrayNumber(int searchNumber, List<Integer> arrayList) {
        this.foundedIndex = findIndex(arrayList, searchNumber);
    }

    private int findIndex(List<Integer> arrayList, int searchNumber) {
        return arrayList.indexOf(searchNumber);
    }

    private String replaceNumbers(List<Integer> arrayList) {
        System.out.println("Значения массива до сдвига:");
        System.out.println(arrayList.toString());

        Comparator<Integer> signValueComparator = new SignValuesComparator();
        arrayList.sort(signValueComparator);

        return arrayList.toString();
    }

    private List<Integer> randomStreamGenerator(int[] array) {
        return new Random().ints(array.length, 0, 50).boxed().collect(Collectors.toList());
    }
}
