package homeworks.homework03.functions;

import utils.menu.Menu;
import utils.menu.MenuOption;

/** Основной класс для запуска программы */
public class Main {
    /**
     * Метод начала работы программы
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        System.out.println("*Работа с массивами*");
        System.out.println();

        Menu menu = new Menu();
        ArrayUtils arrayUtils = new ArrayUtils();

        menu.add(
                new MenuOption("1", "Найти индекс числа в массиве") {
                    /** Метод заполняет пункт меню и описывает действие к выполнению */
                    @Override
                    public void doAction() {
                        FunctionActions.printFoundedIndex(arrayUtils);
                    }
                });

        menu.add(
                new MenuOption("2", "Переместить ненулевые значения массива влево.") {
                    /** Метод заполняет пункт меню и описывает действие к выполнению */
                    @Override
                    public void doAction() {
                        FunctionActions.replaceNotZeroValues(arrayUtils);
                    }
                });
        menu.loopUntilExit();
    }
}
