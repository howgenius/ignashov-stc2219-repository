package homeworks.homework05.members;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import utils.menu.Menu;
import utils.menu.MenuOption;

/** Основной класс для запуска программы */
public class Main {
    private static int result;
    /**
     * Метод начала работы программы
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество элементов массива: ");
        var arraySize = scanner.nextInt();

        ArrayList<Integer> arrayList = new ArrayList<>();

        Menu mainMenu = new Menu();
        Menu subMenu = new Menu(1);

        mainMenu.add(
                new MenuOption("1", "Ввод элементов вручную") {
                    /** Добавляет элементы в коллекцию вручную */
                    @Override
                    public void doAction() {
                        manualArrayInput(arrayList, arraySize, scanner);
                        printArrayElementsAndMenu(arrayList, subMenu);
                    }
                });

        mainMenu.add(
                new MenuOption("2", "Заполнение случайными числами [0-1000]") {
                    /** Заполняет элементы коллекции случайными числами */
                    @Override
                    public void doAction() {
                        randomFillElements(arrayList, arraySize);
                        printArrayElementsAndMenu(arrayList, subMenu);
                    }
                });

        subMenu.add(
                new MenuOption("1", "Сложение всех чисел") {
                    /** Возвращает сумму чисел */
                    @Override
                    public void doAction() {
                        System.out.println("Сумма всех чисел: " + addition(arrayList));
                    }
                });

        subMenu.add(
                new MenuOption("2", "Вычитание всех чисел") {
                    /** Возвращает разность чисел */
                    @Override
                    public void doAction() {
                        System.out.println("Разность всех чисел: " + subtraction(arrayList));
                    }
                });

        subMenu.add(
                new MenuOption("3", "Умножение всех чисел") {
                    /** Возвращает умножение чисел */
                    @Override
                    public void doAction() {
                        System.out.println("Произведение всех чисел: " + multiplication(arrayList));
                    }
                });

        subMenu.add(
                new MenuOption("4", "Деление всех чисел") {
                    /** Возвращает деление чисел */
                    @Override
                    public void doAction() {
                        System.out.println("Частное всех чисел: " + division(arrayList));
                    }
                });

        subMenu.add(
                new MenuOption("5", "Факториал") {
                    /** Возвращает факториал */
                    @Override
                    public void doAction() {
                        System.out.println("Факториал: " + getFactorial(arrayList));
                    }
                });

        mainMenu.loopUntilExit();
    }

    private static void randomFillElements(ArrayList<Integer> arrayList, int arraySize) {
        arrayList.clear();
        Random random = new Random();
        for (int i = 0; i < arraySize; i++) {
            arrayList.add(random.nextInt(1000));
        }
    }

    private static void manualArrayInput(
            ArrayList<Integer> arrayList, int arraySize, Scanner scanner) {
        arrayList.clear();
        for (int i = 0; i < arraySize; i++) {
            System.out.print("Введите элемент массива, под индексом " + i + ": ");
            arrayList.add(scanner.nextInt());
        }
    }

    private static void printArrayElementsAndMenu(ArrayList<Integer> arrayList, Menu subMenu) {
        System.out.println("Элементы массива: " + arrayList);
        subMenu.loopUntilExit();
    }

    /**
     * Статический метод, возвращает сложение чисел, переданных в массиве, в параметрах
     *
     * @param arrayList передаваемая коллекция чисел
     * @return число типа int
     */
    public static int addition(ArrayList<Integer> arrayList) {
        return arrayList.stream().mapToInt(listNumber -> listNumber).sum();
    }

    /**
     * Статический метод, возвращает разность чисел, переданных в массиве, в параметрах
     *
     * @param arrayList передаваемая коллекция чисел
     * @return число типа int
     */
    public static int subtraction(ArrayList<Integer> arrayList) {
        arrayList.forEach(
                element -> {
                    if (arrayList.indexOf(element) == 0) {
                        result = element;
                    } else {
                        result -= element;
                    }
                });

        return result;
    }

    /**
     * Статический метод, возвращает произведение чисел, переданных в массиве, в параметрах
     *
     * @param arrayList передаваемая коллекция чисел
     * @return число типа int
     */
    public static int multiplication(ArrayList<Integer> arrayList) {
        arrayList.forEach(
                element -> {
                    if (arrayList.indexOf(element) == 0) {
                        result = element;
                    } else {
                        result *= element;
                    }
                });

        return result;
    }

    /**
     * Статический метод, возвращает частное чисел, переданных в массиве, в параметрах
     *
     * @param arrayList передаваемая коллекция чисел
     * @return число типа int
     */
    public static int division(ArrayList<Integer> arrayList) {
        for (Integer element : arrayList) {
            if (arrayList.indexOf(element) == 0) {
                result = element;
            } else if (element == 0) {
                System.out.println("Нельзя делить на 0");
                break;
            } else if (result < element) {
                break;
            } else if (element < 0) {
                break;
            } else {
                result /= element;
            }
        }

        return result;
    }

    /**
     * Статический метод, возвращает факториал чисел, переданных в массиве, в параметрах
     *
     * @param arrayList передаваемая коллекция чисел
     * @return число типа int
     */
    public static int getFactorial(ArrayList<Integer> arrayList) {
        int result = 1;

        for (int i = 1; i <= arrayList.size(); i++) {
            result *= i;
        }

        return result;
    }
}
