package utils.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** Класс для создания консольного меню */
public class Menu {
    private static final String OPTION_EXIT = "0";
    private static final String LEVEL_INDENTATION = "    ";
    private final List<MenuOption> menuOption = new ArrayList<>();
    private int level = 0;

    /** Пустой конструктор класса Menu */
    public Menu() {}
    /**
     * Конструктор класса Menu, для создания консольного меню с иерархией
     *
     * @param level уровень иерархии
     */
    public Menu(int level) {
        this.level = level;
    }

    /**
     * Метод добавления строки меню
     *
     * @param m объект класса MenuOption
     */
    public void add(MenuOption m) {
        this.menuOption.add(m);
    }

    /** Метод для отображения меню */
    public void loopUntilExit() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (true) {
                System.out.println(this);
                String input = reader.readLine();
                if (input.equals(OPTION_EXIT)) {
                    return;
                }
                boolean foundOption = false;
                for (MenuOption m : menuOption) {
                    if (m.getOption().equalsIgnoreCase(input)) {
                        m.doAction();
                        foundOption = true;
                    }
                }
                if (!foundOption) {
                    System.out.println("Извините, не удалось распознать пункт меню");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Переопределение метода toString(), вывод приветсвия.
     *
     * @return строка приветствия
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        System.out.println("****************************************************************");
        sb.append(
                "Выберите пункт меню, "
                        + OPTION_EXIT
                        + " для выхода или перехода в предыдущее меню.\n");
        for (MenuOption m : menuOption) {
            sb.append(LEVEL_INDENTATION.repeat(Math.max(0, level)));
            sb.append(m).append("\n");
        }
        return sb.toString();
    }
}
