/**
 * Package with: classes - Menu, MenuOption.
 *
 * @author Maxim Ignashov (mailto:maxster062@yandex.ru)
 * @version 1.0
 * @since 07.10.22
 */
package utils.menu;
