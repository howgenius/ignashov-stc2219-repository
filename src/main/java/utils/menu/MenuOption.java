package utils.menu;

/** Абстрактный метод для вывода опций меню */
public abstract class MenuOption {
    private final String option;
    private final String menuLine;

    /**
     * Конструктор класса
     *
     * @param option номер опционального меню, по которому происходит выбор
     * @param menuLine строка, определяющая наименование опции
     */
    public MenuOption(String option, String menuLine) {
        this.option = option;
        this.menuLine = menuLine;
    }

    /**
     * Переопределение метода toString() для вывода пункта меню
     *
     * @return строка пункта меню
     */
    @Override
    public String toString() {
        return this.option + " - " + this.menuLine;
    }

    /**
     * Getter для защищенного поля option
     *
     * @return option номер опционального меню
     */
    public String getOption() {
        return option;
    }

    /** Абстрактный метод на выполнения действия меню */
    public abstract void doAction();
}
