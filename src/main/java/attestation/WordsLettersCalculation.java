package attestation;

/**
 * Interface WordsLettersCalculation.
 *
 * @author Maxim Ignashov (mailto:maxster062@yandex.ru)
 * @version 1.0
 * @since 07.10.22
 */
public interface WordsLettersCalculation {
    /**
     * Calculate counts of words.
     *
     * @param inputString input string need to calculate words
     * @param pathName input string to file *.txt
     */
    void wordsCount(String inputString, String pathName);

    /**
     * Calculate counts of letters.
     *
     * @param inputString input string need to calculate letters
     * @param pathName input string to file *.txt
     */
    void lettersCount(String inputString, String pathName);
}
