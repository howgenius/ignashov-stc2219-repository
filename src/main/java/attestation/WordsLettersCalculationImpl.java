package attestation;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/** Class for calculate words and letters implements WordsLettersCalculation */
public class WordsLettersCalculationImpl implements WordsLettersCalculation {

    private final File RESULT_FILE_LEVEL_ONE =
            new File("src/main/resources/resultFileLevelOne.txt");
    private final File RESULT_FILE_LEVEL_TWO =
            new File("src/main/resources/resultFileLevelTwo.txt");

    private final HashMap<String, Integer> hashWordsMap = new LinkedHashMap<>();

    @Override
    public void wordsCount(String inputString, String pathName) {
        System.out.println("Введите путь к файлу для подсчёта слов в строке: \n");
        writeFile(new File(pathName), inputString);
        splitInputString(inputString);
        writeFile(RESULT_FILE_LEVEL_ONE, outputString());
    }

    @Override
    public void lettersCount(String inputString, String pathName) {
        System.out.println("Введите путь к файлу для подсчёта букв строки: \n");
        writeFile(new File(pathName), inputString);
        splitInputString(Arrays.toString(inputString.toCharArray()));
        writeFile(RESULT_FILE_LEVEL_TWO, outputString());
    }

    /**
     * Class for writing data in file on disk
     *
     * @param writableFile file to write
     * @param writableString string to write on file
     */
    private void writeFile(File writableFile, String writableString) {
        try (Writer writer = new FileWriter(writableFile)) {
            if ((writableFile.createNewFile()) || writableFile.exists()) {
                writer.write(writableString);
            }
        } catch (IOException e) {
            throw new RuntimeException("Не удалось записать данные в файл.");
        }
    }

    /**
     * Return String after calculating
     *
     * @return string after calculating
     */
    private String outputString() {
        return hashWordsMap.entrySet().stream()
                .map(e -> e.getKey() + " - " + e.getValue())
                .collect(joining("\n"));
    }

    /**
     * Split words and letters with regular expression and sort data by ASC
     *
     * @param inputString input string to calculate
     */
    private void splitInputString(String inputString) {
        Arrays.stream(inputString.toLowerCase().split("[^A-Za-zА-Яа-я]"))
                .filter(word -> word.length() > 0)
                .forEach(word -> hashWordsMap.put(word, hashWordsMap.getOrDefault(word, 0) + 1));
        sortKeyByASC();
    }

    /** Sort keys Map by ASC */
    private void sortKeyByASC() {
        List<Map.Entry<String, Integer>> list = new LinkedList<>(hashWordsMap.entrySet());
        list.sort(Map.Entry.comparingByKey());

        hashWordsMap.clear();
        list.forEach(entry -> hashWordsMap.put(entry.getKey(), entry.getValue()));
    }
}
