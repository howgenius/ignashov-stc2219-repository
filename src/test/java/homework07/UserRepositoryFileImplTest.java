package homework07;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

import homeworks.homework07.User;
import homeworks.homework07.UserRepositoryFileImpl;
import java.io.File;
import java.io.IOException;
import org.junit.Test;

public class UserRepositoryFileImplTest {

    @Test
    public void whenCreateUser() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();

        createNewFile(userRepository);
        checkCreatedUsers(userRepository);
    }

    @Test
    public void whenFindUserById() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();
        createNewFile(userRepository);
        checkCreatedUsers(userRepository);
    }

    @Test
    public void whenUpdateUser() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();
        createNewFile(userRepository);

        User editedUser = userRepository.findById(1);
        editedUser.setName("Владимир");
        editedUser.setAge(27);
        userRepository.update(editedUser);

        checkUpdateUsers(userRepository);
    }

    @Test
    public void whenDeleteUserById() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();
        createNewFile(userRepository);
        userRepository.delete(1);
        checkDeletedUsers(userRepository);
    }

    @Test
    public void whenDeleteUserByIdException() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();
        createNewFile(userRepository);

        Exception exception =
                assertThrows(IllegalArgumentException.class, () -> userRepository.delete(-1));

        assertThat(exception.getMessage(), is("Идентификатор пользователя не найден!"));
    }

    @Test
    public void whenFindUserByIdException() {
        UserRepositoryFileImpl userRepository = new UserRepositoryFileImpl();
        createNewFile(userRepository);
        Exception exception =
                assertThrows(IllegalArgumentException.class, () -> userRepository.findById(3));

        assertThat(exception.getMessage(), is("Введённый id не найден!"));
    }

    private static void checkCreatedUsers(UserRepositoryFileImpl userRepository) {
        User user1 = userRepository.findById(1);
        User user2 = userRepository.findById(2);

        String userToString1 = "1|Иезекииль|Булыжников|33|true";
        String userToString2 = "2|Василий|Камушкин|23|false";

        assertThat(user1.toString(), is(userToString1));
        assertThat(user2.toString(), is(userToString2));
    }

    private static void checkUpdateUsers(UserRepositoryFileImpl userRepository) {
        User user1 = userRepository.findById(1);
        User user2 = userRepository.findById(2);

        String userToString1 = "1|Владимир|Булыжников|27|true";
        String userToString2 = "2|Василий|Камушкин|23|false";

        assertThat(user1.toString(), is(userToString1));
        assertThat(user2.toString(), is(userToString2));
    }

    private static void checkDeletedUsers(UserRepositoryFileImpl userRepository) {
        User user = userRepository.findById(2);
        String userToString = "2|Василий|Камушкин|23|false";
        assertThat(user.toString(), is(userToString));
    }

    private static void createNewFile(UserRepositoryFileImpl userRepository) {
        File file = userRepository.getConstFILE();
        if (file.delete()) {
            try {
                if (file.createNewFile()) {
                    userRepository.create(new User("Иезекииль", "Булыжников", 33, true));
                    userRepository.create(new User("Василий", "Камушкин", 23, false));
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
