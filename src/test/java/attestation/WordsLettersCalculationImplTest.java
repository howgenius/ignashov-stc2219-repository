package attestation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.util.Scanner;

import homeworks.homework07.UserRepositoryFileImpl;
import org.junit.Test;

public class WordsLettersCalculationImplTest {
    @Test
    public void whenSplitWords() {
        String input = "src/main/resources/inputFileLevelOne.txt";
        Scanner scanner = new Scanner(input);
        WordsLettersCalculationImpl wordsLettersOperations = new WordsLettersCalculationImpl();
        wordsLettersOperations.wordsCount(
                "Дифференциация интегрированного параллелограмма "
                        + "с точки зрения фантасмагорической апперцепции",
                scanner.nextLine());

        InputStream is;
        is = this.getClass().getResourceAsStream("/resultFileLevelOne.txt");
        String result = convertStreamToString(is);

        assertAll(
                () -> assertNotNull(is),
                () ->
                        assertThat(
                                result,
                                is(
                                        """
                        апперцепции - 1
                        дифференциация - 1
                        зрения - 1
                        интегрированного - 1
                        параллелограмма - 1
                        с - 1
                        точки - 1
                        фантасмагорической - 1""")));
    }

    @Test
    public void whenSplitLetters() {
        String input = "src/main/resources/inputFileLevelTwo.txt";
        Scanner scanner = new Scanner(input);
        WordsLettersCalculationImpl wordsLettersOperations = new WordsLettersCalculationImpl();
        wordsLettersOperations.lettersCount("Д+ифф!!ере?циация", scanner.nextLine());
        InputStream is;
        is = this.getClass().getResourceAsStream("/resultFileLevelTwo.txt");
        String result = convertStreamToString(is);

        assertAll(
                () -> assertNotNull(is),
                () ->
                        assertThat(
                                result,
                                is(
                                        """
                         а - 1
                         д - 1
                         е - 2
                         и - 3
                         р - 1
                         ф - 2
                         ц - 2
                         я - 1""")));
    }

    @Test
    public void writeFileRuntimeException() {
        String input = "B:/notMatchFile.txt";
        Scanner scanner = new Scanner(input);
        WordsLettersCalculationImpl wordsLettersOperations = new WordsLettersCalculationImpl();

        Exception exception =
                assertThrows(RuntimeException.class, () ->
                        wordsLettersOperations.lettersCount("Д+ифф!!ере?циация", scanner.nextLine()));

        assertThat(exception.getMessage(), is("Не удалось записать данные в файл."));
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
