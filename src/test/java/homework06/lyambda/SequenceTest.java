package homework06.lyambda;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import homeworks.homework06.lyambda.Sequence;
import org.junit.Test;

/**
 * Тесты лямбда выражений
 *
 * @version 1.0
 */
public class SequenceTest {
    /**
     * Когда введён массив чётных и нечетных чисел, возвращает массив чётных, при этом нечетные
     * числа становятся 0.
     */
    @Test
    public void whenArrayInputEvenAndOddNumbersCheckArrayEven() {
        Sequence arrayCheckEvenSequence = new Sequence();

        int[] inputArray = new int[] {23, 16, 8, 1, 5, 6, 8, 44, 11, 88};
        int[] expectedArray = new int[] {0, 16, 8, 0, 0, 6, 8, 44, 0, 88};
        int[] resultArray = arrayCheckEvenSequence.arrayCheckEven(inputArray);

        assertThat(resultArray, is(expectedArray));
    }

    /**
     * Когда введён массив чётных и нечетных чисел, возвращает массив суммы чисел и проверяет на
     * четность, при этом нечетные числа становятся 0.
     */
    @Test
    public void whenArrayInputEvenAndOddNumbersCheckSumArrayEven() {
        Sequence arrayCheckEvenSequence = new Sequence();

        int[] inputArray = new int[] {23, 16, 8, 1, 5, 6, 8, 44, 11, 88};
        int[] expectedArray = new int[] {0, 0, 8, 0, 0, 6, 8, 44, 11, 88};
        int[] resultArray = arrayCheckEvenSequence.arrayCheckSumAtEven(inputArray);

        assertThat(resultArray, is(expectedArray));
    }

    /**
     * Когда введён массив чётных и нечетных чисел, возвращает массив суммы чисел и проверяет на
     * четность, при этом нечетные числа становятся 0.
     */
    @Test
    public void whenArrayInputEvenAndOddNumbersCheckEachNumberAtEven() {
        Sequence arrayCheckEvenSequence = new Sequence();

        int[] inputArray = new int[] {23, 16, 8, 1, 5, 6, 8, 44, 11, 88};
        int[] expectedArray = new int[] {23, 16, 8, 0, 0, 6, 8, 44, 0, 88};
        int[] resultArray = arrayCheckEvenSequence.arrayCheckAllNumbersAtEven(inputArray);

        assertThat(resultArray, is(expectedArray));
    }

    /**
     * Когда введён массив чётных и нечетных чисел, проверяет число на палиндромность, при этом
     * число не палиндром становятся 0.
     */
    @Test
    public void whenArrayInputNumbersCheckPalindrom() {
        Sequence arrayCheckEvenSequence = new Sequence();

        int[] inputArray = new int[] {23, 16, 8, 1, 5, 6, 8, 44, 11, 88};
        int[] expectedArray = new int[] {0, 0, 8, 1, 5, 6, 8, 44, 11, 88};
        int[] resultArray = arrayCheckEvenSequence.arrayCheckAllNumbersPalindrom(inputArray);

        assertThat(resultArray, is(expectedArray));
    }
}
